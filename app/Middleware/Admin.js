'use strict'
/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const Role = use('App/Models/Role');
const Database = use('Database')

class Admin {
  /**
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Function} next
   */
  async handle ({ request }, next) {
    // const arr = [];
    // return arr;
    // await Role.query().select().fetch()

    // const super_admin_ids = await Database.from('roles').where('name', 'Super-Admin').orWhere('name', 'Admin').
    // orWhere('name', 'Moderator').first()

    // return super_admin_ids;
    await next()
  }
}

module.exports = Admin

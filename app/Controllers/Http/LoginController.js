'use strict'
const { validate } = use('Validator')
const Customer = use('App/Models/Vendor')
const Partner = use('App/Models/Partner')


class LoginController {

    
    async login ({ auth, request,view,response }) {
        if(request.method() === 'POST')
        {
            const { email, password } = request.all()
            // if (auth.user instanceof Customer || auth.user instanceof Partner) {
                // user is customer or partner
                await auth.attempt(email, password)
                return 'Logged in successfully';
            // }
            // return 'Wrong Login Details';
            
        }
        return view.render('views.login');
    }

    async logout({auth,response}){
        await auth.logout();
        return response.redirect('/');
    }
}

module.exports = LoginController

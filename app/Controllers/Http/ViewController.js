'use strict'
const { validate } = use('Validator')
const Contact = use('App/Models/Contact');

class ViewController {

    async index({view})
    {
        return view.render('partials.content');
    }

    async faq({view})
    {
        return view.render('views.faq');
    }

    async about({view})
    {
        return view.render('views.about');
    }

    async blog({view})
    {
        return view.render('views.blog');
    }

    async contact({request, response, session,view})
    {   
        if (request.method() === 'POST') {
            const validation = await validate(request.all(), {
                name: 'required',
                subject: 'required',
                email: 'required|unique:contacts',
                message: 'required',
            })
        //    return request.all();
            // show error messages upon validation fail
            if (validation.fails()) {
                session.withErrors(validation.messages())
                    .flashAll()
                return response.redirect('back')
            }
            const contact = new Contact()
            contact.name = request.input('name')
            contact.subject = request.input('subject')
            contact.email = request.input('email')
            contact.message = request.input('message')
            await contact.save()

            // Fash success message to session
            session.flash({ notification: 'Contact added!' })
            return response.redirect('back')
            
        }
        return view.render('views.contact');
    }

    

    async forget_password({request, response, session,view}){
    
        if (request.method() === 'POST') {
            const validation = await validate(request.all(), {
                email: 'required',
                login: 'required',
            })
            // show error messages upon validation fail
            if (validation.fails()) {
                session.withErrors(validation.messages())
                    .flashAll()
                return response.redirect('back')
            }
            // store request to database
            const vendor = new Vendor()
            vendor.bank = request.input('bank')
            vendor.acct_number = request.input('acct_number')
            await vendor.save()

            // Fash success message to session
            session.flash({ notification: 'Contact added!' })
            return response.redirect('back')

        }
        return view.render('views.login');
    }

    async partner({request, response, session,view})
    {
        if (request.method() === 'POST') {
            const validation = await validate(request.all(), {
                first_name: 'required',
                last_name: 'required',
                phone_number: 'required',
                email: 'required|unique',
                address: 'required',
                locality: 'required',
                city: 'required',
                state: 'required',
                bank: 'required',
                acct_number: 'required',
            })
            // show error messages upon validation fail
            if (validation.fails()) {
                session.withErrors(validation.messages())
                    .flashAll()
                return response.redirect('back')
            }
            // store request to database
            const vendor = new Vendor()
            vendor.first_name = request.input('first_name')
            vendor.last_name = request.input('last_name')
            vendor.phone_number = request.input('phone_number')
            vendor.email = request.input('email')
            vendor.address = request.input('address')
            vendor.locality = request.input('locality')
            vendor.city = request.input('city')
            vendor.bank = request.input('bank')
            vendor.acct_number = request.input('acct_number')
            await vendor.save()

            // Fash success message to session
            session.flash({ notification: 'Contact added!' })
            return response.redirect('back')

        }
        return view.render('views.partner');
    }

    async form(request,response,session,view){
        if(request.method() === 'POST'){
            const validation = await validate(request.all(), {
                first_name: 'required',
                last_name: 'required',
                phone_number: 'required',
                email: 'required|unique',
                address: 'required',
                locality: 'required',
                city: 'required',
                state: 'required',
                bank: 'required',
                acct_number: 'required',
            })
        }
        return view.render('views.forms');

    }
    async vendor({request, response, session,view})
    {
        if (request.method() === 'POST') {
            const validation = await validate(request.all(), {
                first_name: 'required',
                last_name: 'required',
                phone_number: 'required',
                email: 'required|unique',
                address: 'required',
                locality: 'required',
                city: 'required',
                state: 'required',
                bank: 'required',
                acct_number: 'required',
            })
            // show error messages upon validation fail
            if (validation.fails()) {
                session.withErrors(validation.messages())
                    .flashAll()
                return response.redirect('back')
            }
            // store request to database
            const vendor = new Vendor()
            vendor.first_name = request.input('first_name')
            vendor.last_name = request.input('last_name')
            vendor.phone_number = request.input('phone_number')
            vendor.email = request.input('email')
            vendor.address = request.input('address')
            vendor.locality = request.input('locality')
            vendor.city = request.input('city')
            vendor.bank = request.input('bank')
            vendor.acct_number = request.input('acct_number')
            await vendor.save()

            // Fash success message to session
            session.flash({ notification: 'Contact added!' })
            return response.redirect('back')

        }
        return view.render('views.vendor');
    }
}

module.exports = ViewController

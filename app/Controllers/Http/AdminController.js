'use strict'
const { validate } = use('Validator')
class AdminController {
    async login({request, auth, response, session, view}){
        if(request.method() === 'POST'){
            const { email, password,} = request.all();

            try {
                await auth.attempt(email, password);
                session.flash({ success: 'Login Succefully' })
                return response.redirect('/');            
            }
             catch (error) {
                // session.flash({ error: 'Check Your Credentials!' })
                session.flash({ error: error.message })
                return response.route('admin.login')
            }
        }
        return view.render('Admin.login');
    }

   

    async home({view})
    {
        return view.render('Admin.dashboard');
    }
}

module.exports = AdminController

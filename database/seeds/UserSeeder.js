'use strict'

/*
|--------------------------------------------------------------------------
| UserSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const User = use('App/Models/User');
const Hash = use('Hash')
const Encryption = use('Encryption')

class UserSeeder {
  async run () {
    const encrypted = Encryption.encrypt('password')
    await User.create({
      username: 'luqman',
      role_id: 1,
      'email':'oluwatosinolamilekan@gmail.com',
      'password':'olamilekan11',
    })
    await User.create({
      username: 'nurudeen',
      role_id: 2,
      'email':'admin@gmail.com',
      'password':'password',
    })
    await User.create({
      username: 'sk',
      role_id: 3,
      'email':'superadmin@gmail.com',
      'password':'password',
    })
  }
}

module.exports = UserSeeder

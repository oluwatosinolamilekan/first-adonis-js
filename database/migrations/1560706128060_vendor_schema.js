'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class VendorSchema extends Schema {
  up () {
    this.create('vendors', (table) => {
      table.increments()
      table.string('first_name')
      table.string('last_name')
      table.string('phone_number')
      table.string('email').unique()
      table.string('address')
      table.string('locality')
      table.string('city')
      table.string('state')
      table.string('bank')
      table.string('acct_number')
      table.timestamps()
    })
  }

  down () {
    this.drop('vendors')
  }
}

module.exports = VendorSchema

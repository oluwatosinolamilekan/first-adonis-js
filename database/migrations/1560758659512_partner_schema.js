'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PartnerSchema extends Schema {
  up () {
    this.create('partners', (table) => {
      table.increments()
      table.string('first_name')
      table.string('last_name')
      table.string('phone_number')
      table.string('email').unique()
      table.string('password')
      table.string('address')
      table.string('locality')
      table.string('city')
      table.string('state')
      table.timestamps()
    })
  }

  down () {
    this.drop('partners')
  }
}

module.exports = PartnerSchema

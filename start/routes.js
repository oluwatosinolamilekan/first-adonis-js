'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

// Route.on('/').render('partials.content')
Route.get('/','ViewController.index').as('index');
Route.get('/faq','ViewController.faq').as('faq');
Route.get('/about','ViewController.about').as('about');
Route.get('/blog','ViewController.blog').as('blog');
// Route.any('/contact','ViewController.contact').as('contact');
Route.route('/contact', 'ViewController.contact', ['GET', 'POST']).as('contact');
Route.route('/partner', 'ViewController.partner', ['GET', 'POST']).as('partner');
Route.route('/form', 'ViewController.form', ['GET', 'POST']).as('form');

Route.route('/login', 'LoginController.login', ['GET', 'POST']).as('login');
Route.get('/logout','LoginController.logout').as('logout');

Route.route('/forget_password', 'ViewController.forget_password', ['GET', 'POST']).as('forget_password');
Route.any('/vendor','ViewController.vendor').as('vendor');

// Route.get(...).middleware('auth:admin')
Route.route('/login/admin', 'AdminController.login', ['GET', 'POST']).as('admin.login');







